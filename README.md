
# Building the Video Capturing container

# Building the Video Capturing container

## Introduction
This document guides the user to build and deploy the video capturing container.

## Pre-Requisites
- ARM Based Sysytem Ready Platform. \
  The following platforms have been tested on: 
    - Rockchip 3399ProD
    - Radxa Rock4B  
    - NXP i.MX 8M Plus

- USB Drive Flashed with ARM System-Ready EWAOL Image \
  Or any other Generic Arm64 image with docker installed.

- Video Souce:
  * Video.mp4 file: \
    This is optional but recommended as it enables the user to deploy the use-case application \
    with minimal hardware components as well as test in the cloud. 
  * USB webcam: \
    The demo also works with a USB camera as input source device. you can point the camera to a \
    live video or traffic footage to provide input frames to the application

Note: that the docker container can run either with a usb webcam or a MP4 video file. \
Choose the most appropriate one.


## Steps overview
- Clone the repository
- Building and running the docker container with usb webcam
- Building and running the docker container with MP4 video file
- Next steps

## Clone the repository

- Clone the gitlab repo using below command
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
## Building the docker container
- Change the working directory to Video capturing contianer folder
```
$ cd <PROJECT_ROOT>
```
- Build the docker container
```
$ docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
```
- Update IMAGE_NAME and IMAGE_TAG. These can be given according to user choice eg. Video_capturing_container:v1

## Running the docker container with usb webcam
- Use the below command to run the docker container to take input from usb webcam, update the image name and tag accordingly
```
$ docker run -it --network=host --device=/dev/video0 <IMAGAE_NAME>:<IMAGE_TAG> ./mediamtx usb_webcam.yml 
```
## Running the docker container with MP4 video file
- Copy a video file inside the project folder
```
$ cp <PATH_TO_MP4_VIDEO_FILE> <ROOT_OF_PROJECT_FOLDER>/video.mp4
```
- Open the Dockerfile and add a COPY instruction to copy the video file to container and save the file.
```
COPY <PATH_TO_MP4_VIDEO_FILE> video.mp4
```
- Build the docker container
```
$ docker build -t rtsp-server:latest .
```
- Run the docker contaienr
```
$ docker run -it --network=host <IMAGAE_NAME>:<IMAGE_TAG> ./mediamtx mp4.yml 
``` 
This will start the RTSP streaming server at port number 8554/cam, the user can play the stream with below link
```
rtsp://127.0.0.1:8554/cam
```

## Next steps
- Clone and build the inference container.
