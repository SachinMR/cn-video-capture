FROM aler9/rtsp-simple-server AS rtsp
FROM alpine:3.12
RUN apk add --no-cache ffmpeg
COPY --from=rtsp /mediamtx /
COPY mediamtx.yml /mediamtx.yml
COPY mp4.yml .
COPY usb_webcam.yml .
# COPY video.mp4 /video.mp4

